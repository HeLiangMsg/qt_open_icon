#include "iconhelper.h"
#include "muifont.h"

IconHelper *IconHelper::_instance=0;

IconHelper::IconHelper()
{
    //默认用mui图标
    iconFont = MuiFont().getIconFont();
}

void IconHelper::setNewIcon(const AbstractFont &newFont)
{
    iconFont = newFont.getIconFont();
}


QPixmap iconhelp::transFontToPixmap(const QFont &newFont, int size, int fontSize, int iconIndex)
{
    return transFontToPixmap(newFont, size, size, fontSize, iconIndex);
}

QPixmap iconhelp::transFontToPixmap(const QFont &newFont, int w, int h, int fontSize, int iconIndex)
{
    QLabel widget;
    widget.setAttribute(Qt::WA_TranslucentBackground);
    widget.setFixedSize(w, h);
    widget.setAlignment(Qt::AlignCenter);
    widget.setText(QChar(iconIndex));

    QFont font = newFont;
    font.setPointSize(fontSize);
    widget.setFont(font);

    return QPixmap::grabWidget(&widget, widget.rect());
}
