#-------------------------------------------------
#
# Project created by QtCreator 2016-06-03T09:56:02
#
#-------------------------------------------------

QT       += core gui network sql
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets serialport

TARGET = qt_open_icon
TEMPLATE = app

INCLUDEPATH += \
    font \
    core \
    utils \
    common \
    test \
    utils/label \
    utils/button \
    utils/animation \
    utils/dialog \
    utils/timeset \
    utils/wheel \
    utils/navbar \
    utils/table \
    utils/desktop \
    utils/desktop/procedure \
    utils/input \
    utils/progressbar \
    utils/titlebar \
    utils/animationlist \
    service \
    service/database \
    service/database/connectionpool \
    service/database/dao \
    service/database/entity \
    service/database/logic \
    tools

#DEFINES += RUN_EMBEDDED  #是否运行于嵌入式设备


win32:msvc{
    CONFIG +=c++11 no_batch
}else{
    QMAKE_CXXFLAGS += -std=c++11
    QMAKE_CXXFLAGS += -Wunused-function
    QMAKE_CXXFLAGS += -Wunused-parameter
    QMAKE_CXXFLAGS += -Wreorder
    QMAKE_CXXFLAGS += -Wswitch
}

SOURCES += main.cpp \
    test/mainwindow.cpp \
    core/iconhelper.cpp \
    font/abstractfont.cpp \
    font/fontawesomewebfont.cpp \
    font/muifont.cpp \
    font/timefont.cpp \
    common/global.cpp \
    common/utilscommon.cpp \
    common/randomname.cpp \
    utils/button/basebutton.cpp \
    utils/button/buttontest.cpp \
    utils/button/switchbutton.cpp \
    utils/button/batterybutton.cpp \
    utils/button/groupbutton.cpp \
    utils/button/checkable.cpp \
    utils/button/radiobutton.cpp \
    utils/button/checkbutton.cpp \
    utils/button/togglebutton.cpp \
    utils/button/mybuttontest.cpp \
    utils/animation/abstractanimation.cpp \
    utils/animation/animationcenter.cpp \
    utils/animation/animationcover.cpp \
    utils/animation/animationfly.cpp \
    utils/animation/animationsharp.cpp \
    utils/animation/animationslide.cpp \
    utils/animation/animationtest.cpp \
    utils/animation/animationwidget.cpp \
    utils/dialog/notifymanager.cpp \
    utils/dialog/toast.cpp \
    utils/dialog/dialogtest.cpp \
    utils/dialog/basemessagebox.cpp \
    utils/label/baselabel.cpp \
    utils/label/labeltest.cpp \
    utils/label/marqueelabel.cpp \
    utils/label/marqueelabeltest.cpp \
    utils/progressbar/progressstate.cpp \
    utils/progressbar/testprogress.cpp \
    utils/progressbar/progressbar2.cpp \
    utils/progressbar/circularprogressbar.cpp \
    utils/progressbar/loadingprogressbar.cpp \
    utils/progressbar/ringsprogressbar.cpp \
    utils/progressbar/roundprogressbar.cpp \
    utils/progressbar/myprogresstest.cpp \
    utils/navbar/navbar.cpp \
    utils/navbar/navbartest.cpp \
    utils/desktop/desktop.cpp \
    utils/desktop/procedure/qqprocedure.cpp \
    utils/desktop/procedure/procedure.cpp \
    utils/table/baselist.cpp \
    utils/table/listtest.cpp \
    utils/table/normallist.cpp \
    utils/table/linedelegate.cpp \
    utils/table/navlist.cpp \
    utils/input/digitalinput.cpp \
    utils/input/digitalinputtest.cpp \
    utils/input/lineeditwidget.cpp \
    utils/input/lineeditwidgettest.cpp \
    utils/animationlist/mylistwidget.cpp \
    utils/titlebar/titlebar.cpp \
    service/database/connectionpool/connectionpool.cpp \
    service/database/dao/abstractdao.cpp \
    service/database/dao/studentdaoimpl.cpp \
    service/database/entity/studentdata.cpp \
    service/database/logic/databasewig.cpp \
    tools/encrypt/aes/qaesencryption.cpp \
    tools/serialdebug/serialdebug.cpp \
    utils/joystick/joystick.cpp


HEADERS  += \
    test/mainwindow.h \
    core/iconhelper.h \
    font/abstractfont.h \
    font/fontawesomewebfont.h \
    font/muifont.h \
    font/timefont.h \
    common/global.h \
    common/utilscommon.h \
    common/randomname.h \
    common/platformhead.h \
    utils/button/basebutton.h \
    utils/button/buttontest.h \
    utils/button/switchbutton.h \
    utils/button/batterybutton.h \
    utils/button/groupbutton.h \
    utils/button/checkable.h \
    utils/button/radiobutton.h \
    utils/button/checkbutton.h \
    utils/button/togglebutton.h \
    utils/button/mybuttontest.h \
    utils/animation/abstractanimation.h \
    utils/animation/animationcenter.h \
    utils/animation/animationcover.h \
    utils/animation/animationfly.h \
    utils/animation/animationsharp.h \
    utils/animation/animationslide.h \
    utils/animation/animationtest.h \
    utils/animation/animationwidget.h \
    utils/dialog/notifymanager.h \
    utils/dialog/toast.h \
    utils/dialog/dialogtest.h \
    utils/dialog/basemessagebox.h \
    utils/label/baselabel.h \
    utils/label/labeltest.h \
    utils/label/marqueelabel.h \
    utils/label/marqueelabeltest.h \
    utils/progressbar/progressstate.h \
    utils/progressbar/testprogress.h \
    utils/progressbar/progressbar2.h \
    utils/progressbar/circularprogressbar.h \
    utils/progressbar/loadingprogressbar.h \
    utils/progressbar/ringsprogressbar.h \
    utils/progressbar/roundprogressbar.h \
    utils/progressbar/myprogresstest.h \
    utils/navbar/navbar.h \
    utils/navbar/navbartest.h \
    utils/desktop/desktop.h \
    utils/desktop/procedure/qqprocedure.h \
    utils/desktop/procedure/procedure.h \
    utils/table/baselist.h \
    utils/table/listtest.h \
    utils/table/normallist.h \
    utils/table/linedelegate.h \
    utils/table/navlist.h \
    utils/input/digitalinput.h \
    utils/input/digitalinputtest.h \
    utils/input/lineeditwidget.h \
    utils/input/lineeditwidgettest.h \
    utils/animationlist/mylistwidget.h \
    utils/titlebar/titlebar.h \
    service/database/connectionpool/connectionpool.h \
    service/database/dao/abstractdao.h \
    service/database/dao/studentdaoimpl.h \
    service/database/entity/studentdata.h \
    service/database/logic/databasewig.h \
    tools/encrypt/aes/qaesencryption.h \
    tools/serialdebug/serialdebug.h \
    utils/joystick/joystick.h


RESOURCES += \
    resource.qrc

equals(QT_MAJOR_VERSION, 5){
    HEADERS += \
        utils/timeset/headutils.h \
        utils/timeset/timetotal.h \
        utils/timeset/timedisplay.h \
        utils/timeset/switchwidget.h \
        utils/timeset/timeset.h \
        utils/timeset/zoneset.h \
        utils/timeset/zonetable.h \
        utils/wheel/wheelwidget.h \
        utils/wheel/stringwheelwidget.h \
        utils/wheel/wheeltest.h

    SOURCES += \
        utils/timeset/headutils.cpp \
        utils/timeset/timetotal.cpp \
        utils/timeset/timedisplay.cpp \
        utils/timeset/switchwidget.cpp \
        utils/timeset/timeset.cpp \
        utils/timeset/zoneset.cpp \
        utils/timeset/zonetable.cpp \
        utils/wheel/wheelwidget.cpp \
        utils/wheel/stringwheelwidget.cpp \
        utils/wheel/wheeltest.cpp
}

equals(QT_MAJOR_VERSION, 4){
CONFIG += gcc serialport
}


#message(parse over)

FORMS += \
    test/mainwindow.ui \
    utils/progressbar/progressbar2.ui \
    utils/progressbar/testprogress.ui \
    utils/progressbar/myprogresstest.ui \
    utils/label/labeltest.ui \
    utils/label/marqueelabeltest.ui \
    utils/navbar/navbartest.ui \
    utils/button/buttontest.ui \
    utils/button/mybuttontest.ui \
    utils/input/lineeditwidgettest.ui \
    service/database/logic/databasewig.ui \
    tools/serialdebug/serialdebug.ui

HEADERS += \
    common/gbk.h
